# MA ANA #

A small tool for allowing the curious to peek into the mysterious around us.

### Tell Me more... ###

We originally created this tool, to aid in exploring the typically overlooked, but undoubtedly essential relationship between language, numbers, the seen and the unseen. It's an experiment, but thus far, we feel, this has potential to unlock some applications not easily possible before. 

You are welcome to contribute, fork and exploit this tech ;-)