package com.nuchwezi.maana.maana;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TextView;

import java.util.ArrayList;

public class NumberActivity extends AppCompatActivity {

    public static final String TAG = "MAANA";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_numbers);

        initQuery();
    }

    private void initQuery() {
        EditText editText = findViewById(R.id.eTxtQuery);
        editText.requestFocus();
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.showSoftInput(editText, InputMethodManager.SHOW_IMPLICIT);
    }

    public void solveAnagram(View view){
        EditText eTxtQuery = findViewById(R.id.eTxtQuery);
        String query = eTxtQuery.getText().toString().trim();
        if(query.length() == 0){
            return;
        }

        TextView txtSolution = findViewById(R.id.txtSolution);
        String anagram_solution  = Anagrams.solve_numeric(query);
        String gematria_solution = Kaballah.solve(anagram_solution);
        txtSolution.setText(String.format("%s\n\n%s\n***\n%s : %s",anagram_solution, gematria_solution, Kaballah.solve2(anagram_solution), Kaballah.solve3(anagram_solution)));

        ArrayList<Integer> solutionColors = Pallete.mapToColors(gematria_solution, 27);


        LinearLayout palleteContainer = findViewById(R.id.pallete);
        palleteContainer.removeAllViews();

        for(int color: solutionColors){
            TextView txt = new TextView(this);
            txt.setText(" ");
            txt.setBackgroundColor(color);
            TableLayout.LayoutParams llp = new TableLayout.LayoutParams(TableLayout.LayoutParams.MATCH_PARENT, TableLayout.LayoutParams.WRAP_CONTENT, 1f);
            txt.setLayoutParams(llp);
            palleteContainer.addView(txt);
        }

        Integer solution_color_blend = Pallete.mapToColorBlend(gematria_solution, 27);
        LinearLayout palleteBlendColorContainer = findViewById(R.id.palleteBlend);
        palleteBlendColorContainer.removeAllViews();
        TextView txt = new TextView(this);
        txt.setText(" ");
        txt.setBackgroundColor(solution_color_blend);
        TableLayout.LayoutParams llp = new TableLayout.LayoutParams(TableLayout.LayoutParams.MATCH_PARENT, TableLayout.LayoutParams.WRAP_CONTENT, 1f);
        txt.setLayoutParams(llp);
        palleteBlendColorContainer.addView(txt);

        Integer solution_color_average = Pallete.mapToColorAverage(gematria_solution, 27);
        LinearLayout palleteAverageColorContainer = findViewById(R.id.palleteAverage);
        palleteAverageColorContainer.removeAllViews();
        TextView txt2 = new TextView(this);
        txt2.setText(" ");
        txt2.setBackgroundColor(solution_color_average);
        txt2.setLayoutParams(llp);
        palleteAverageColorContainer.addView(txt2);

        InputMethodManager imm = (InputMethodManager) getSystemService(this.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(eTxtQuery.getWindowToken(), 0);
    }

    private void showAbout() {

        Utility.showAlert(
                this.getString(R.string.app_name),
                String.format("Version %s (Build %s)\n\n%s",
                        Utility.getVersionName(this),
                        Utility.getVersionNumber(this),
                        this.getString(R.string.powered_by)),
                R.mipmap.ic_launcher, this);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main_numbers, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId()) {
            /*case R.id.action_settings: {
                //showSettings();
                //forceReload = true;
                return true;
            }*/
            case R.id.action_words: {
                switchToWORDS();
                return true;
            }
            case R.id.action_about: {
                showAbout();
                return true;
            }
        }



        return super.onOptionsItemSelected(item);
    }


    private void switchToWORDS() {
        Intent _intent = new Intent(NumberActivity.this, WordActivity.class);
        startActivity(_intent);
        finish();
    }


}
