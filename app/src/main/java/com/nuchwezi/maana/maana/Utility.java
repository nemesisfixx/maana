package com.nuchwezi.maana.maana;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.util.Log;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by AK1N Nemesis Fixx on 2/16/2018.
 */

class Utility {

    private static String Tag = WordActivity.TAG;

    /*
    * Display a toast with the default duration : Toast.LENGTH_SHORT
    */
    public static void showToast(String message, Context context) {
        showToast(message, context, Toast.LENGTH_SHORT);
    }

    /*
     * Display a toast with given Duration
     */
    public static void showToast(String message, Context context, int duration) {
        Toast.makeText(context, message, duration).show();
    }

    public static void showAlert(String title, String message, Context context) {
        showAlert(title, message, R.mipmap.ic_launcher, context, null, null,null);
    }

    public static void showAlert(String title, String message, int iconId, Context context) {
        showAlert(title, message, iconId, context,  null, null,null);
    }

    public static void showAlert(String title, String message, Context context, Runnable yesCallback,  Runnable noCallback, Runnable cancelCallback ) {
        showAlert(title, message, R.mipmap.ic_launcher, context, yesCallback, noCallback,cancelCallback);
    }

    public static void showAlert(String title, String message, int iconId, Context context, Runnable yesCallback,  Runnable noCallback, Runnable cancelCallback ) {
        showAlertFactory(title, message,iconId, context, yesCallback, noCallback,cancelCallback);
    }


    public static void showAlertFactory(String title, String message, int iconId,
                                        Context context, final Runnable yesCallback, final Runnable noCallback, final Runnable cancelCallback) {
        try {
            AlertDialog.Builder builder = new AlertDialog.Builder(context);

            builder.setIcon(iconId);
            builder.setTitle(title);
            builder.setMessage(message);

            if(yesCallback != null){
                builder.setPositiveButton( noCallback == null ? "OK"  : "YES", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        yesCallback.run();
                    }
                });
            }

            if(noCallback != null){
                builder.setNegativeButton("NO", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        noCallback.run();
                    }
                });
            }

            if(cancelCallback != null){
                builder.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        cancelCallback.run();
                    }
                });
            }

            AlertDialog alert = builder.create();
            alert.show();
        } catch (Exception e) {
            Log.e(Tag, "Alert Error : " + e.getMessage());
        }

    }


    public static int getVersionNumber(Context context) {
        PackageInfo pinfo = null;
        try {
            pinfo = context.getPackageManager().getPackageInfo(
                    context.getPackageName(), 0);
        } catch (PackageManager.NameNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return pinfo != null ? pinfo.versionCode : 1;
    }

    public static String getVersionName(Context context) {
        PackageInfo pinfo = null;
        try {
            pinfo = context.getPackageManager().getPackageInfo(
                    context.getPackageName(), 0);
        } catch (PackageManager.NameNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return pinfo != null ? pinfo.versionName : "DEFAULT";
    }


    // Map
    public interface IMapper<V, T> {

        T each(V i);

    }

    public static <V, T> List<T> map(IMapper<V, T> processor, V[] input) {
        List<T> o = new ArrayList<>();
        for(V i : input) {
            o.add(processor.each(i));
        }
        return o;
    }

    // Reduce
    public interface IReducer<V, T> {

        T reduce(T c, V i);

    }

    public static <V, T> T reduce(IReducer<V, T> processor, V[] input, T initiator) {
        T o = initiator;
        for(V i : input) {
            o = processor.reduce(o, i);
        }
        return o;
    }

}
