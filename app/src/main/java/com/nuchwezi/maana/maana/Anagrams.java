package com.nuchwezi.maana.maana;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

/**
 * Created by AK1N Nemesis Fixx on 2/16/2018.
 */

class Anagrams {
    public static String solve(String query) {

        ArrayList queryl = new ArrayList<String>(Arrays.asList(query.trim().split("")));
        Collections.shuffle(queryl);

        StringBuilder sb = new StringBuilder(queryl.size());
        for(int s = 0; s < queryl.size(); s++){
            sb.append(queryl.get(s));
        }

        return sb.toString();
    }

    public static String solve_numeric(String numeric_query) {
        return solve(Kaballah.numbers_to_letters(numeric_query));
    }
}
