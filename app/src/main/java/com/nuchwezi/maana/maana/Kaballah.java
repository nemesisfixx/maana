package com.nuchwezi.maana.maana;

import android.text.TextUtils;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Random;

/**
 * Created by AK1N Nemesis Fixx on 2/17/2018.
 */

class Kaballah {

    static final List<Character> alphabet = Utility.map(new Utility.IMapper<String,Character>() {
        @Override
        public Character each(String i) {
            return new Character(i.charAt(0));
        }
    },"a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z, ".split(","));

    /**
     * Returns the numeric equivalent of each letter in the input string
     */
    public static String solve(String query) {

        List<Character> c_query = new ArrayList<>();

        List<Integer> numbers = new ArrayList<>();

        for(String s: query.toLowerCase().trim().split("")){
            if(s.isEmpty())
                continue;

            numbers.add(alphabet.indexOf(s.charAt(0))+1);
        }

        StringBuilder sb = new StringBuilder(numbers.size());
        sb.append(TextUtils.join(" ", numbers));

        return sb.toString();
    }

    /**
    Returns the unreduced numeric value of the input string
     */
    public static int solve2(String query) {
        String solution = solve(query);
        int sum = 0;

        for(String s: solution.trim().split(" ")){
            if(s.isEmpty())
                continue;

           sum += Integer.parseInt(s);
        }

       return sum;
    }

    /**
    Returns the reduced numeric value of the input string
     */
    public static int solve3(String query) {
        int solution = solve2(query);
        return (solution % 9);
    }

    public static String numbers_to_letters(String numeric_query) {

        ArrayList<Integer> parsedNumbers = new ArrayList<>();
        for(String s: numeric_query.trim().split(" ")){
            if(s.isEmpty())
                continue;

            if(s == " ")
                parsedNumbers.add(alphabet.indexOf(" "));

            Random random = new Random();
            for(int i = 0; i < s.length(); i++){
                // sometimes we could read read a single digit, or read two digits at once, to get a multi-digit index
                boolean read2Digits = random.nextBoolean();
                if(read2Digits){
                    if((i+1) < s.length()) {
                        int val = Integer.parseInt(s.substring(i, i + 2));
                        if(val < alphabet.size()) {
                            // the 2 digit index does't exceed our limit
                            parsedNumbers.add(val);
                            i += 1;
                        }else {
                            // we have no option but to read the first of the two digits
                            parsedNumbers.add(Integer.parseInt(s.substring(i, i+1)));
                        }
                    }
                    else{
                        // we are at the end of the string, can only read 1 more
                        parsedNumbers.add(Integer.parseInt(s.substring(i, i+1)));
                    }
                }else {
                    // read a single digit
                    parsedNumbers.add(Integer.parseInt(s.substring(i, i+1)));
                }
            }

        }

        StringBuilder sb = new StringBuilder();
        for(Integer i: parsedNumbers){
            if(i == 0)
                sb.append(" "); // we consider 0 to be a blank space.
            else if( i > alphabet.size())
                continue;
            else {
                sb.append(alphabet.get(i - 1)); // we deduct 1, so 1 can correctly map to the first letter at index 0
            }
        }

        return sb.toString();
    }
}
