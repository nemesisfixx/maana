package com.nuchwezi.maana.maana;

import android.graphics.Color;
import android.support.v4.graphics.ColorUtils;

import java.util.ArrayList;

/**
 * Created by AK1N Nemesis Fixx on 2/18/2018.
 */

class Pallete {
    public static ArrayList<Integer> mapToColors(String solution, int limit) {
        ArrayList<Integer> colors = new ArrayList<>();
        for(String s: solution.trim().split(" ")){
            try {
                if (s.isEmpty())
                    continue;
                int n = Integer.parseInt(s);
                float angle = Float.valueOf(String.valueOf(((n * 1.0) / limit) * 360));
                int color = ColorUtils.HSLToColor(new float[]{angle, n == limit ? 0 : 1, .5f});
                colors.add(color);
            }catch (Exception e){
                colors.add(Color.BLACK);
            }
        }

        return colors;
    }

    public static Integer mapToColorBlend(String solution, int limit) {
        ArrayList<float[]> colors = new ArrayList<>();
        for(String s: solution.trim().split(" ")){
            try {
                if (s.isEmpty())
                    continue;
                int n = Integer.parseInt(s);
                float angle = Float.valueOf(String.valueOf(((n * 1.0) / limit) * 360));
                colors.add(new float[]{angle, n == limit ? 0 : 1, .5f});
            }catch (Exception e){
                //colors.add(Color.BLACK);
            }
        }

        if(colors.size() == 1)
            return ColorUtils.HSLToColor(colors.get(0));
        else if(colors.size() > 1){
            float[] finalColor = new float[3];

            for(int c = 1; c < colors.size(); c++){
                ColorUtils.blendHSL(colors.get(c-1), colors.get(c), .5f, finalColor);
            }
            return ColorUtils.HSLToColor(finalColor);

        }else
            return Color.BLACK;
    }

    public static Integer mapToColorAverage(String solution, int limit) {
        ArrayList<float[]> colors = new ArrayList<>();
        for(String s: solution.trim().split(" ")){
            try {
                if (s.isEmpty())
                    continue;
                int n = Integer.parseInt(s);
                float angle = Float.valueOf(String.valueOf(((n * 1.0) / limit) * 360));
                colors.add(new float[]{angle, n == limit ? 0 : 1, .5f});
            }catch (Exception e){
                //colors.add(Color.BLACK);
            }
        }

        if(colors.size() == 1)
            return ColorUtils.HSLToColor(colors.get(0));
        else if(colors.size() > 1){
            float[] finalColor = colors.get(0);
            for(int c = 1; c < colors.size(); c++){
                float[] hsl = colors.get(c);
                finalColor[0] += hsl[0];
                finalColor[1] += hsl[1];
                finalColor[2] += hsl[2];
            }

            finalColor[0] = finalColor[0] / colors.size();
            finalColor[1] = finalColor[1] / colors.size();
            finalColor[2] = finalColor[2] / colors.size();

            return ColorUtils.HSLToColor(finalColor);
        }else
            return Color.BLACK;
    }
}
